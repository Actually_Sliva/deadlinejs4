const REQUIRED_PROPS = ['episodeId', 'name', 'openingCrawl'];
const REQUIRED_KEY = 'characters';
const BLOCK_NAME = 'movie';

function createHTMLElement(tagName = "div", className, value, attr = []) {
    const elem = document.createElement(tagName);
    if (className) {
        elem.classList.add(className)
    }
    if (value !== undefined) {
        elem.innerHTML = value;
    }
    attr.forEach(obj => {
        if (obj instanceof Object) {
            elem.setAttribute(Object.entries(obj)[0][0], Object.entries(obj)[0][1])
        }
    })
    return elem;
}

function insertSpinner(insertIn) {
    insertIn.insertAdjacentHTML('beforeend', `
        <div id="bars">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>    
    `)
}

function insertBlock(obj, parentTag, childrenTag, parentClass, props, id, insertIn) {
    const box = createHTMLElement(parentTag, parentClass, '', [{ 'id': id }])
    props.forEach(prop => {
        const item = createHTMLElement(childrenTag, `${parentClass}__${prop}`, `<strong class='${parentClass}__${prop}--strong'>${prop}</strong>: ${obj[prop]}`)
        box.append(item)
    })
    const list = createHTMLElement('ul', `${parentClass}__${REQUIRED_KEY}-list`)
    box.append(list)
    insertSpinner(box)
    insertIn.append(box)
}

function getData(url) {
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error(error))
}

function getMultipleData(arr) {
    let requests = arr.map(url => getData(url))
    return Promise.all(requests)
}

getData('https://ajax.test-danit.com/api/swapi/films')
    .then(arr => arr.forEach(obj => {
        const id = `${REQUIRED_PROPS[0]}-${obj[REQUIRED_PROPS[0]]}`

        insertBlock(obj, 'div', 'p', BLOCK_NAME, REQUIRED_PROPS, id, document.querySelector('#root'))

        getMultipleData(obj[REQUIRED_KEY])
            .then(data => {
                document.querySelector('#bars').remove()
                data.forEach(item => {
                    const listInBlock = document.querySelector(`#${id} .${BLOCK_NAME}__${REQUIRED_KEY}-list`)
                    const listItem = createHTMLElement('li', `${BLOCK_NAME}__${REQUIRED_KEY.slice(0, -1)}`, item['name'])
                    listInBlock.append(listItem)
                })
            })
            .catch(error => console.error(error))
    }))
    .catch(error => console.error(error))





// function insertElements(obj, parentTag, childrenTag, parentClass, props, insertIn) {
//     const box = createHTMLElement(parentTag, parentClass, '', [{ 'id': `${props[0]}-${obj[props[0]]}` }])
//     props.forEach(prop => {
//         const item = createHTMLElement(childrenTag, `${parentClass}__${prop}`, `<strong class='${parentClass}__${prop}--strong'>${prop}</strong>: ${obj[prop]}`)
//         box.append(item)
//     })
//     insertIn.append(box)
// }

// function getData(url) {
//     return fetch(url)
//         .then(response => response.json())
//         .then(response => response)
//         .catch(error => console.error(error.message))
// }

// function getMultipleData(arr) {
//     let requests = arr.map(url => fetch(url))
//     return Promise.all(requests)
//         .then(responses => Promise.all(responses.map(response => response.json())))
// }


// function insertMovie(obj) {
//     insertElements(obj, 'div', 'p', 'movie', requiredProps, document.querySelector('#root'))
//     document.querySelector(`#${requiredProps[0]}-${obj[requiredProps[0]]}`).insertAdjacentHTML('beforeend', `<ul class="movie__characters-list"></ul>`)
// }

// function insertCharacters(obj, value) {
//     document.querySelector(`#${requiredProps[0]}-${obj[requiredProps[0]]} .movie__characters-list`).append(createHTMLElement('li', `movie__character`, value))
// }

// getData('https://ajax.test-danit.com/api/swapi/films')
//     .then(arr => arr.forEach(obj => {
//         insertElements(obj, 'div', 'p', 'movie', requiredProps, document.querySelector('#root'))
//         getMultipleData(obj[requiredKey])
//             .then(data => data.forEach(item => {
//                 document.querySelector(`#${requiredProps[0]}-${obj[requiredProps[0]]}`).append(createHTMLElement('span', `movie__character`, item['name']))
//             }))
//     }))


// function promiseAll(arr) {
//     let requests = arr.map(url => fetch(url))
//     Promise.all(requests)
//         .then(responses => {
//             responses.forEach(response => response.json().then(data=>{
//                 insertElements(data, 'div', 'span', 'characters', ['name'], document.querySelector('.movie'))
//             }))
//         })
// }

// getData('https://ajax.test-danit.com/api/swapi/films', requiredProps, 'characters')